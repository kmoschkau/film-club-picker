import requests
import collections
import urllib.request
from bs4 import BeautifulSoup

def get_user_list(username, list_type):
    url = "https://letterboxd.com/" + str(username) + "/" + list_type
    result = requests.get(url)
    page = 1
    film_list = []
     
    while (True):
        page = page + 1
        soup = BeautifulSoup(result.content, "html.parser")
        posters = soup.find_all('li', class_='poster-container')
        if len(posters) == 0:
            break
        try: 
            for poster in posters:
                img = poster.find('img')
                film_list.append(img['alt'])
        except Exception as e:
            print(e)
        url = "https://letterboxd.com/" + str(username) + "/" + list_type + "/page/" + str(page)

        result = requests.get(url)
    return film_list

def process_usernames(usernames):
    seenlist = []
    for username in usernames:
        user_seenlist = get_user_list(username, "films")
        seenlist = seenlist + list(set(user_seenlist) - set(seenlist))

    watchlists = []
    for username in usernames:
        watchlist = get_user_list(username, "watchlist")
        watchlists.append(watchlist)

    counter = collections.Counter(sum(watchlists, []))
    new_watchlist = {}
    for name, amount in counter.most_common():
        if name not in seenlist and amount>1:
           new_watchlist[name] = amount
    
    return new_watchlist

def main():

    usernames = ["sideways_stairs", "Kenn_Xav", "JonButler", "coryboy6", "kal9000", "otterbox_12"]

    new_watchlist = process_usernames(usernames)

    for name in new_watchlist.keys():
        print('\'%s\' is in %s lists and nobody has seen it.' % (name, new_watchlist[name]))

if __name__ == "__main__":
    main()
        





